<?php
include ('inc/header.php');
$main_template = 'row_code.tpl';
$smarty->assign('page_title','QR kodo peržiūra');
$stmt = $db->prepare('SELECT * FROM kodai
				WHERE kodoID=:kodas AND vartotojas=:vartotojas');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->bindValue(':vartotojas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kodo_turinys = $stmt->fetch(PDO::FETCH_ASSOC);


$stmt = $db->prepare('SELECT unikalios+neunikalios as suma ,unikalios,neunikalios FROM perziuros
				WHERE kodai_kodoID=:kodas AND diena=:data');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
$stmt->execute();
$perziuros_siandien = $stmt->fetch(PDO::FETCH_ASSOC);

if ($perziuros_siandien['suma']=='') {
	$perziuros_siandien['suma']='0';
}
if ($perziuros_siandien['unikalios']=='') {
	$perziuros_siandien['unikalios']='0';
}
if ($perziuros_siandien['neunikalios']=='') {
	$perziuros_siandien['neunikalios']='0';
}
//var_dump($perziuros_siandien);


$stmt = $db->prepare('SELECT * FROM operacines
				WHERE kodai_kodoID=:kodas
				ORDER BY kiekis DESC
				LIMIT 10');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
$operacines = $stmt->fetchAll(PDO::FETCH_ASSOC);


$stmt = $db->prepare('SELECT * FROM narsykles
				WHERE kodai_kodoID=:kodas
				ORDER BY kiekis DESC
				LIMIT 10');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
$narsykles = $stmt->fetchAll(PDO::FETCH_ASSOC);

$stmt = $db->prepare('SELECT * FROM salys
				WHERE kodai_kodoID=:kodas
				ORDER BY kiekis DESC
				LIMIT 10');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
$salys = $stmt->fetchAll(PDO::FETCH_ASSOC);

$smarty->assign('kodas',$kodo_turinys);
$smarty->assign('operacines', $operacines);
$smarty->assign('salys', $salys);
$smarty->assign('narsykles',$narsykles);
$smarty->assign('perziuros_siandien',$perziuros_siandien);

$smarty->assign('main_template', $main_template);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>