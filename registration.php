<?php
include ('inc/header.php');
$main_template = 'row_registration.tpl';
$smarty->assign('page_title','Registracijos puslapis');
$errors = array();
$sucess_register = false;
if (sizeof($_POST)>0) {
	$errors['firma'] = empty($_POST['firma']);
	$errors['adresas'] = empty($_POST['adresas']);
	$errors['prisijungimas'] = empty($_POST['prisijungimas']);
	$errors['el_pastas'] = (filter_var($_POST['el_pastas'], FILTER_VALIDATE_EMAIL)==false);
	$errors['svetaine'] = (filter_var($_POST['svetaine'], FILTER_VALIDATE_URL)==false);
	$errors['kodas'] = (filter_var($_POST['kodas'], FILTER_VALIDATE_INT)==false) or empty($_POST['kodas']);
	$errors['slaptazodis'] = !(strlen($_POST['slaptazodis'])>=5);

	if ($errors['prisijungimas']===false) {
		$stmt = $db->prepare('SELECT * FROM vartotojai WHERE prisijungimo_vardas=:prisijungimas');
		$stmt->bindValue(':prisijungimas', $_POST['prisijungimas'], PDO::PARAM_STR);
		$stmt->execute();
		$row_count = $stmt->rowCount();
		if ($row_count>0) $errors['prisijungimas'] = true;
	}

	$errors['total'] = ($errors['firma'] or $errors['adresas'] or $errors['el_pastas'] or $errors['svetaine'] or $errors['kodas'] or $errors['slaptazodis'] or $errors['prisijungimas']);

	if ($errors['total']===false) {
		$sucess_register = true;
		$insert = $db->prepare('INSERT INTO vartotojai(prisijungimo_vardas,prisijungimo_slaptazodis,imone,el_pastas,svetaine,kodas) VALUES (:prisijungimas,:slaptazodis,:firma,:el_pastas,:svetaine,:kodas)');
		$insert->bindValue(':prisijungimas', $_POST['prisijungimas'], PDO::PARAM_STR);
		$insert->bindValue(':slaptazodis', $_POST['slaptazodis'], PDO::PARAM_STR);
		$insert->bindValue(':firma', $_POST['firma'], PDO::PARAM_STR);
		$insert->bindValue(':el_pastas', $_POST['el_pastas'], PDO::PARAM_STR);
		$insert->bindValue(':svetaine', $_POST['svetaine'], PDO::PARAM_STR);
		$insert->bindValue(':kodas', $_POST['kodas'], PDO::PARAM_STR);
		$insert->execute();
	}
}
$smarty->assign('main_template', $main_template);
$smarty->assign('errors', $errors);
$smarty->assign('sucess_register', $sucess_register);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>