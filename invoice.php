<?php
include ('inc/header.php');
$main_template = 'row_invoice.tpl';
$smarty->assign('page_title','Sąskaitos peržiūra');
$stmt = $db->prepare('SELECT * FROM saskaitos
				WHERE saskaitosID=:saskaita AND vartotojas=:vartotojas');
$stmt->bindValue(':saskaita', $_GET['id'], PDO::PARAM_INT);
$stmt->bindValue(':vartotojas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$saskaitos_turinys = $stmt->fetch(PDO::FETCH_ASSOC);

$stmt = $db->prepare('SELECT * FROM vartotojai
				WHERE prisijungimo_vardas=:vartotojas');
$stmt->bindValue(':vartotojas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kliento_turinys = $stmt->fetch(PDO::FETCH_ASSOC);

$stmt = $db->prepare('SELECT * FROM kodai
				WHERE kodoID=:kodas AND vartotojas=:vartotojas');
$stmt->bindValue(':kodas', $saskaitos_turinys['kodas'], PDO::PARAM_INT);
$stmt->bindValue(':vartotojas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kodo_turinys = $stmt->fetch(PDO::FETCH_ASSOC);

$smarty->assign('saskaita',$saskaitos_turinys);
$smarty->assign('kodas',$kodo_turinys);
$smarty->assign('klientas',$kliento_turinys);
$smarty->assign('main_template', $main_template);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>