<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require('libs/Smarty.class.php');
$smarty = new Smarty;
$smarty->setTemplateDir('templates');
$smarty->setCompileDir('templates_c');
$smarty->setCacheDir('cache');
$smarty->caching = false;
$smarty->cache_lifetime = 120;
$logged = false;
try {
  $db = new PDO("mysql:host=localhost;dbname=studentai_reliz", 'studentai_reliz', 'reliz');
  $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}
catch(PDOException $e) {
    $smarty->assign('error','KLAIDA su duombaze.');
}

if ($_COOKIE['login'] and $_COOKIE['password']) {
	$stmt = $db->prepare('SELECT * FROM vartotojai WHERE prisijungimo_vardas=:prisijungimas AND prisijungimo_slaptazodis=:slaptazodis');
	$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
	$stmt->bindValue(':slaptazodis', $_COOKIE['password'], PDO::PARAM_STR);
	$stmt->execute();
	$row_count = $stmt->rowCount();
	if ($row_count>0) $logged = true;
	else { 
		setcookie("login", "", time()-36000);
		setcookie("password", "", time()-36000);
		header("location:index.php");
	}
}

$smarty->assign('logged',$logged);
?>