<?php
include ('inc/header.php');
$main_template = 'row_invoices_list.tpl';
$smarty->assign('page_title','Sąskaitų sąrašas');
$stmt = $db->prepare('SELECT * FROM saskaitos
					WHERE vartotojas=:prisijungimas');
$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();

//print_r($stmt->fetchAll(PDO::FETCH_ASSOC));

$smarty->assign('saskaitos',$stmt->fetchAll(PDO::FETCH_ASSOC));

$smarty->assign('main_template', $main_template);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>