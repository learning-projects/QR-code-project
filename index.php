<?php
include('inc/header.php');
$main_template = 'row_main.tpl';

$stmt = $db->prepare('SELECT SUM(perziuros.unikalios) AS unikalus, SUM(perziuros.neunikalios) AS neunikalus, SUM(perziuros.unikalios+perziuros.neunikalios) AS viso, kodai.vartotojas, kodai.kodoID
					FROM kodai
					INNER JOIN perziuros ON kodai.kodoID=perziuros.kodai_kodoID
					WHERE kodai.vartotojas=:prisijungimas');
$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$apsilankymai = $stmt->fetch(PDO::FETCH_ASSOC);

if ($apsilankymai['unikalus']=='') {
	$apsilankymai['unikalus'] = '0';
}
if ($apsilankymai['neunikalus']=='') {
	$apsilankymai['neunikalus'] = '0';
}
if ($apsilankymai['viso']=='') {
	$apsilankymai['viso'] = '0';
}

$stmt = $db->prepare('SELECT COUNT(*) AS aktyvus
					FROM kodai
					WHERE kodai.vartotojas=:prisijungimas AND busena=\'aktyvus\'');
$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kodai['aktyvus'] = $stmt->fetch(PDO::FETCH_ASSOC);

$kodai['aktyvus'] = $kodai['aktyvus']['aktyvus'];

$stmt = $db->prepare('SELECT COUNT(*) AS viso
					FROM kodai
					WHERE kodai.vartotojas=:prisijungimas');
$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kodai['viso'] = $stmt->fetch(PDO::FETCH_ASSOC);

$kodai['viso'] = $kodai['viso']['viso'];

$stmt = $db->prepare('SELECT COUNT(*) AS laukia
					FROM kodai
					WHERE kodai.vartotojas=:prisijungimas AND busena=\'Laukia apmokejimo\'');
$stmt->bindValue(':prisijungimas', $_COOKIE['login'], PDO::PARAM_STR);
$stmt->execute();
$kodai['laukia'] = $stmt->fetch(PDO::FETCH_ASSOC);

$kodai['laukia'] = $kodai['laukia']['laukia'];

//print_r($kodai);

$smarty->assign('apsilankymai',$apsilankymai);

$smarty->assign('kodai',$kodai);

$smarty->assign('main_template', $main_template);

$smarty->assign('page_title','Pagrindinis puslapis');

$smarty->display('main.tpl');
?>
