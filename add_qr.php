<?php
include ('inc/header.php');
$main_template = 'row_add_qr.tpl';
$smarty->assign('page_title','Pridėti QR kodą');
$errors = array();
//print_r($_POST);
if (sizeof($_POST)>0) {
	$errors['tipas'] = !($_POST['tipas']=='URL');
	$errors['turinys'] = (filter_var($_POST['turinys'], FILTER_VALIDATE_URL)==false);

	$errors['total'] = ($errors['tipas'] or $errors['turinys']);

	if ($errors['total']==false) { 
		$sucess_added = true;
		$insert = $db->prepare('INSERT INTO kodai(tipas,turinys,busena,vartotojas) VALUES (:tipas,:turinys,\'Laukia apmokejimo\',:vartotojas)');
		$insert->bindValue(':tipas', $_POST['tipas'], PDO::PARAM_STR);
		$insert->bindValue(':turinys', $_POST['turinys'], PDO::PARAM_STR);
		$insert->bindValue(':vartotojas', $_COOKIE["login"], PDO::PARAM_STR);
		$insert->execute();
		$lastinserted = $db->lastInsertId();
		$insert = $db->prepare('INSERT INTO saskaitos(saskaitos_suma, apmoketa, vartotojas, kodas) VALUES (\'10\',\'0\',:vartotojas,:kodas)');
		$insert->bindValue(':vartotojas', $_COOKIE["login"], PDO::PARAM_STR);
		$insert->bindValue(':kodas', $lastinserted, PDO::PARAM_STR);
		$insert->execute();
	}
}

$smarty->assign('sucess_added', $sucess_added);
$smarty->assign('main_template', $main_template);
$smarty->assign('errors', $errors);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>