<?php
include ('inc/header.php');
$main_template = 'row_login.tpl';
$smarty->assign('page_title','Prisijungimo puslapis');
$errors = array();
if (sizeof($_POST)>0) {
	$errors['prisijungimas'] = empty($_POST['prisijungimas']);
	$errors['slaptazodis'] = !(strlen($_POST['slaptazodis'])>=5);

	if (($errors['prisijungimas']===false) && ($errors['slaptazodis']===false)) {
		$stmt = $db->prepare('SELECT * FROM vartotojai WHERE prisijungimo_vardas=:prisijungimas AND prisijungimo_slaptazodis=:slaptazodis');
		$stmt->bindValue(':prisijungimas', $_POST['prisijungimas'], PDO::PARAM_STR);
		$stmt->bindValue(':slaptazodis', $_POST['slaptazodis'], PDO::PARAM_STR);
		$stmt->execute();
		$row_count = $stmt->rowCount();
		if ($row_count==0) { 
			$errors['prisijungimas'] = true;
			$errors['slaptazodis'] = true;
		}
	}

	$errors['total'] = ($errors['slaptazodis'] or $errors['prisijungimas']);

	if ($errors['total']===false) {
		setcookie("login",$_POST['prisijungimas'],time()+3600);
		setcookie("password",$_POST['slaptazodis'],time()+3600);
		header("location: index.php");
	}
}
$smarty->assign('main_template', $main_template);
$smarty->assign('errors', $errors);
$smarty->assign('post',array_map('htmlspecialchars', $_POST));

$smarty->display('main.tpl');
?>