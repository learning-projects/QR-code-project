<!---BEGIN SIDEBAR-->
		<div id="sidebar">
			<ul>
				<li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Pagrindinis</span></a></li>
				{if $logged}
				<li><a href="codes.php"><i class="icon icon-qrcode"></i> <span>QR kodai</span></a></li>
				<li><a href="invoices.php"><i class="icon icon-list-alt"></i> <span>Sąskaitos</span></a></li>
				{else}
				<li><a href="registration.php"><i class="icon icon-thumbs-up"></i> <span>Registracija</span></a></li>
				<li><a href="login.php"><i class="icon  icon-share-alt"></i> <span>Prisijungimas</span></a></li>
				{/if}
			</ul>
		</div>
<!--END SIDEBAR-->