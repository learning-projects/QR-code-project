{if $logged}
				<div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box widget-plain">
                            <div class="widget-content center">
                                <ul class="stats-plain">
                                    <li>                                        
                                        <h4>{$perziuros_siandien.suma}</h4>
                                        <span>Apsilankymų šiandien</span>
                                    </li>
                                    <li>                                        
                                        <h4>{$perziuros_siandien.unikalios}</h4>
                                        <span>Unikalių apsilankymų šiandien</span>
                                    </li>
                                    <li>                                        
                                        <h4>{$perziuros_siandien.neunikalios}</h4>
                                        <span>Neunikalių apsilankymų šiandien</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <i class="icon-th-list"></i>
                                </span>
                                <h5>QR kodas</h5>
                            </div>
                            <div class="widget-content">
                                {if $kodas.busena eq 'Aktyvus'}
                                <img src="qr_image.php?id={$kodas.kodoID}" /><br />
                                Kiti dydžiai: <a href="qr_image.php?id={$kodas.kodoID}&amp;size=150" target="_blank">150x150</a>, <a href="qr_image.php?id={$kodas.kodoID}&size=300" target="_blank">300x300</a>, <a href="qr_image.php?id={$kodas.kodoID}&size=500" target="_blank">500x500</a>
                                {else}
                                Kodas dar neaktyvuotas!
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <i class="icon-th-list"></i>
                                </span>
                                <h5>QR kodo duomenys</h5>
                            </div>
                            <div class="widget-content">
                                <b>Tipas:</b> {$kodas.tipas}<br />
                                <b>Būsena:</b> {$kodas.busena}<br />
                                <b>Užkoduotas tekstas:</b> <br />
                                {$kodas.turinys}
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="row-fluid">
                    <div class="span4">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <i class="icon-eye-open"></i>
                                </span>
                                <h5>Top naršyklės (savaitės)</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Naršyklė</th>
                                            <th>Apsilankymų</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$narsykles item=narsykle}
                                        <tr>
                                            <td>{$narsykle.pavadinimas}</td>
                                            <td>{$narsykle.kiekis}</td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <i class="icon-arrow-right"></i>
                                </span>
                                <h5>Operacinės sistemos (savaitės)</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Operacinė</th>
                                            <th>Apsilankymų</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         {foreach from=$operacines item=operacine}
                                        <tr>
                                            <td>{$operacine.pavadinimas}</td>
                                            <td>{$operacine.kiekis}</td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <i class="icon-file"></i>
                                </span>
                                <h5>Šalys (savaitės)</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Šalis</th>
                                            <th>Apsilankymų</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         {foreach from=$salys item=salis}
                                        <tr>
                                            <td>{$salis.pavadinimas}</td>
                                            <td>{$salis.kiekis}</td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>





{/if}