<!--BEGIN CONTENT-->
		<div id="content">
			<!--BEGIN CONTENT HEADER-->
			<div id="content-header">
				<h1>{$page_title}</h1>
				{include file='admin_header.tpl'}
			</div>
			
			<!--END CONTENT HEADER-->
			<!--BEGIN CONTAINER-->
			<div class="container-fluid">
				{include file=$main_template}
				{include file='footer.tpl'}
			</div>
			<!--END CONTAINER-->
		</div>
		<!--END CONTENT-->