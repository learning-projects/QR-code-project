{if !$logged}
				<div class="row-fluid">
					<div class="span12">
						{if $sucess_register}
                        <div class="alert alert-sucess">
                            Registracija sėkminga, galit prisijungti.
                        </div>
                        {else}
                        
                        {if $errors.total}
                        <div class="alert alert-error">
                            Įvyko klaida, ištaisykite.
                        </div>
                        {/if}
                        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>
								</span>
								<h5>Registracija</h5>
							</div>
							<div class="widget-content nopadding">
								<form class="form-horizontal" method="post" action="registration.php" name="registration" id="registration">
                                    <div class="control-group {if $errors.firma}error{/if}">
                                        <label class="control-label">Firma</label>
                                        <div class="controls">
                                            <input type="text" name="firma" id="firma" {if isset($post.firma)}value="{$post.firma}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.adresas}error{/if}">
                                        <label class="control-label">Adresas</label>
                                        <div class="controls">
                                            <input type="text" name="adresas" id="adresas" {if isset($post.adresas)}value="{$post.adresas}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.el_pastas}error{/if}">
                                        <label class="control-label">El. paštas</label>
                                        <div class="controls">
                                            <input type="text" name="el_pastas" id="el_pastas" {if isset($post.el_pastas)}value="{$post.el_pastas}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.svetaine}error{/if}">
                                        <label class="control-label">Svetainė</label>
                                        <div class="controls">
                                            <input type="text" name="svetaine" id="svetaine" {if isset($post.svetaine)}value="{$post.svetaine}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.kodas}error{/if}">
                                        <label class="control-label">Įmonės kodas</label>
                                        <div class="controls">
                                            <input type="text" name="kodas" id="kodas" {if isset($post.kodas)}value="{$post.kodas}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.prisijungimas}error{/if}">
                                        <label class="control-label">Prisijungimo vardas</label>
                                        <div class="controls">
                                            <input type="text" name="prisijungimas" id="prisijungimas" {if isset($post.prisijungimas)}value="{$post.prisijungimas}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.slaptazodis}error{/if}">
                                            <label class="control-label">Slaptažodis</label>
                                            <div class="controls">
                                                <input type="password" name="slaptazodis" id="slaptazodis" {if isset($post.slaptazodis)}value="{$post.slaptazodis}"{/if} />
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" value="Registruotis" class="btn btn-primary" />
                                    </div>
                                </form>
							</div>
						</div>
                        {/if}
					</div>
				</div>

{/if}