{if $logged}
				<div class="row-fluid">
					<div class="span12">
                        {if $sucess_added}
                        <div class="alert alert-sucess">
                            QR kodas sėkmingai sukurtas.
                        </div>
                        {else}
                        {if $errors.total}
                        <div class="alert alert-error">
                            Įvyko klaida, ištaisykite.
                        </div>
                        {/if}
                        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>
								</span>
								<h5>QR kodo kurimas</h5>
							</div>
							<div class="widget-content nopadding">
								<form class="form-horizontal" method="post" action="add_qr.php" name="login" id="login">
                                    <div class="control-group {if $errors.tipas}error{/if}">
                                        <label class="control-label">Tipas</label>
                                        <div class="controls">
                                            <label><input type="radio" name="tipas" id="tipas" value="URL" checked /> URL</label>
                                            <label><input type="radio" name="tipas" disabled /> El. paštas</label>
                                            <label><input type="radio" name="tipas" disabled /> Telefono numeris</label>
                                            <label><input type="radio" name="tipas" disabled /> SMS žinutė</label>
                                            <label><input type="radio" name="tipas" disabled /> Telefono kontaktas</label>
                                            <label><input type="radio" name="tipas" disabled /> Tekstas</label>
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.turinys}error{/if}">
                                            <label class="control-label">Turinys</label>
                                            <div class="controls">
                                                <input type="text" name="turinys" id="turinys" {if isset($post.turinys)}value="{$post.turinys}"{/if} />
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" value="Pridėti" class="btn btn-primary" />
                                    </div>
                                </form>
							</div>
						</div>
                        {/if}
					</div>
				</div>

{/if}