<!--BEGIN ROW-->
				<div class="row-fluid">
					<div class="span12">
						{if isset($error)}
						<!--BEGIN ALERT-->
						<div class="alert alert-error">
							{$error}
						</div>
						<!--END ALERT-->
						{/if}
						{if $logged}
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Bendra statistika</h5><div class="buttons"><a href="index.php" class="btn btn-mini"><i class="icon-refresh"></i> Atnaujinti statistiką</a></div></div>
							<div class="widget-content">
								<div class="row-fluid">
								<div class="span4">
									<ul class="site-stats">
										<li><i class="icon-user"></i> <strong>{$apsilankymai.viso}</strong> <small>Viso apsilankymų</small></li>
										<li><i class="icon-arrow-right"></i> <strong>{$apsilankymai.unikalus}</strong> <small>Unikalių apsilankymų</small></li>
										<li><i class="icon-arrow-right"></i> <strong>{$apsilankymai.neunikalus}</strong> <small>Neunikalių apsilankymų</small></li>
										<li class="divider"></li>
										<li><i class="icon-shopping-cart"></i> <strong>{$kodai.aktyvus}</strong> <small>Aktyvių kodų</small></li>
										<li><i class="icon-tag"></i> <strong>{$kodai.viso}</strong> <small>Viso kodų</small></li>
										<li><i class="icon-repeat"></i> <strong>{$kodai.laukia}</strong> <small>Laukiančių kodų</small></li>
									</ul>
								</div>
								<div class="span8">
									Apsilankymų skaičius skaičiuojamas pastarosiom 7 dienom. Kodų skaičiai yra bendri duomenys.
								</div>
								</div>
							</div>
						</div>
						{else}
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th-list"></i>
								</span>
								<h5>Apie QR reklama</h5>
							</div>
							<div class="widget-content">
								<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, quos, vitae, quae, facilis distinctio delectus nesciunt officia eos temporibus odit repellendus voluptatem totam voluptatum expedita impedit aspernatur praesentium possimus mollitia.</div>
								<div>Expedita, similique, illo blanditiis provident amet ab nemo libero nesciunt pariatur hic cum perferendis molestias eos? Ipsa, ad, cumque, temporibus voluptate expedita adipisci nobis repellat ea vel excepturi tenetur a.</div>
								<div>Totam, voluptatibus, accusantium, officiis similique magni ad accusamus praesentium laudantium sequi dolores earum incidunt provident qui quos exercitationem modi aut omnis necessitatibus reiciendis mollitia. Ex, minima eligendi quibusdam voluptates cumque.</div>
								<div>Harum, ab, officiis sunt quasi delectus laboriosam iure architecto aliquam officia voluptas libero debitis aspernatur esse et rem minus maxime explicabo expedita aut quos eveniet veritatis labore incidunt consequatur tempora.</div>
								<div>Maxime, quasi, excepturi qui et eveniet inventore ullam sapiente blanditiis quidem quis. Ipsum odio minus ut voluptatem fugiat quam veniam! A, enim ad beatae maxime quasi eveniet fugiat corporis neque!</div>
								<div>Sequi, reiciendis, porro quis voluptas asperiores deleniti natus temporibus maiores molestiae eveniet. Harum, aut, in molestiae blanditiis amet earum quisquam accusantium totam aspernatur quod ipsam quas quaerat minus nostrum eligendi.</div>
								<div>Assumenda, nisi, sunt, amet officiis nostrum ipsam cupiditate non laboriosam possimus ipsum asperiores rem recusandae quo! Beatae, aperiam, harum? Minima, quidem, ipsa magni tempora tempore aperiam eum laudantium voluptatum impedit.</div>
							</div>
						</div>
						{/if}
					</div>
				</div>
<!--END ROW-->