{if $logged}
				<div class="row-fluid">
					<div class="span12">
                        <div class="widget-box">
                            <div class="widget-title">
                                <h5>Sąskaitų sąrašas</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                    <th>Sąskaitos ID</th>
                                    <th>Kodo ID</th>
                                    <th>Apmokėta</th>
                                    <th>Peržiūrėti kodą / Apmokėti sąskaitą</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$saskaitos item=saskaita}
                                    <tr>
                                    <td>{$saskaita.saskaitosID}</td>
                                    <td>{$saskaita.kodas}</td>
                                    <td>{if $saskaita.apmoketa eq '0'}NEAPMOKĖTA{else}apmokėta{/if}</td>
                                    <td><a href="qr.php?id={$saskaita.kodas}">Peržiūrėti</a> / 
                                        <a href="invoice.php?id={$saskaita.saskaitosID}">
                                            {if $saskaita.apmoketa eq '0'}Apmokėti{else}Peržiūrėti{/if} sąskaitą
                                        </a></td>
                                    </tr>
                                    {/foreach}
                                    </tbody>
                                    </table>  
                            </div>
                        </div>
					</div>
				</div>

{/if}