{if !$logged}
				<div class="row-fluid">
					<div class="span12">
                        {if $errors.total}
                        <div class="alert alert-error">
                            Įvyko klaida, ištaisykite.
                        </div>
                        {/if}
                        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>
								</span>
								<h5>Prisijungimas</h5>
							</div>
							<div class="widget-content nopadding">
								<form class="form-horizontal" method="post" action="login.php" name="login" id="login">
                                    <div class="control-group {if $errors.prisijungimas}error{/if}">
                                        <label class="control-label">Prisijungimo vardas</label>
                                        <div class="controls">
                                            <input type="text" name="prisijungimas" id="prisijungimas" {if isset($post.prisijungimas)}value="{$post.prisijungimas}"{/if} />
                                        </div>
                                    </div>
                                    <div class="control-group {if $errors.slaptazodis}error{/if}">
                                            <label class="control-label">Slaptažodis</label>
                                            <div class="controls">
                                                <input type="password" name="slaptazodis" id="slaptazodis" {if isset($post.slaptazodis)}value="{$post.slaptazodis}"{/if} />
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" value="Prisijungti" class="btn btn-primary" />
                                    </div>
                                </form>
							</div>
						</div>
					</div>
				</div>

{/if}