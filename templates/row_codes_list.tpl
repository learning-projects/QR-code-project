{if $logged}
				<div class="row-fluid">
					<div class="span12">
                        <div class="widget-box">
                            <div class="widget-title">
                                <h5>QR kodai</h5>
                                <a href="add_qr.php"><span class="label label-info">Pridėti</span></a>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                    <th>Kodo ID</th>
                                    <th>Tipas</th>
                                    <th>Busena</th>
                                    <th>Peržiūrėti/apmokėti sąskaitą</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$kodai item=kodas}
                                    <tr>
                                    <td>{$kodas.kodoID}</td>
                                    <td>{$kodas.tipas}</td>
                                    <td>{$kodas.busena}</td>
                                    <td><a href="qr.php?id={$kodas.kodoID}">Peržiūrėti</a> 
                                        {if $kodas.busena eq 'Laukia apmokejimo'}/ <a href="invoice.php?id={$kodas.saskaitosID}">Apmokėti sąskaitą</a>{/if}
                                    </td>
                                    </tr>
                                    {/foreach}
                                    </tbody>
                                    </table>  
                            </div>
                        </div>
					</div>
				</div>

{/if}