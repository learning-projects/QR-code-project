				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th-list"></i>
								</span>
								<h5>Sąskaita</h5>
								<div class="buttons">
									{if $saskaita.apmoketa eq '0'}<a title="Icon Title" class="btn btn-mini" href="pay.php?id={$saskaita.saskaitosID}"><i class="icon-shopping-cart"></i> Sumokėti</a>{/if}
									<a title="Icon Title" class="btn btn-mini" href="#"><i class="icon-print"></i> Spausdinti</a>
								</div>
							</div>
							<div class="widget-content">
								<div class="invoice-content">
									<div class="invoice-head">
										<div class="invoice-meta">
											Sąskaita <span class="invoice-number">#{$saskaita.saskaitosID} </span>{if $saskaita.apmoketa eq '1'} (APMOKĖTA) {/if}<span class="invoice-date">Date: 2013-06-06</span>
										</div>
										<h5>Sąskaitos siuntėjas/gavėjas</h5>
										<div class="invoice-to">
											<ul>
												<li>
												<span><strong>Nuo</strong></span>
												<span>QR Reklama</span>
												<span>Muitinės g. 8</span>
												<span>Kaunas</span>
												</li>
											</ul>
										</div>
										<div class="invoice-from">
											<ul>
												<li>
												<span><strong>Kam</strong></span>
												<span>{$klientas.imone}</span>
												<span>{$klientas.kodas}</span>
												<span>{$klientas.svetaine}</span>
												</li>
											</ul>
										</div>
									</div>
									<div>
										<table class="table table-bordered">
										<thead>
										<tr>
											<th>
												 Kodo ID
											</th>
											<th>
												 Tipas
											</th>
											<th>
												 Suma
											</th>
										</tr>
										</thead>
										<tfoot>
										<tr>
											<th class="total-label" colspan="2">
												 Viso:
											</th>
											<th class="total-amount">
												 {$saskaita.saskaitos_suma}
											</th>
										</tr>
										</tfoot>
										<tbody>
										<tr>
											<td>
												 #{$kodas.kodoID}
											</td>
											<td>
												 {$kodas.tipas}
											</td>
											<td>
												 {$saskaita.saskaitos_suma}
											</td>
										</tr>
										</tbody>
										</table>
									</div>
									<p class="help-block">
										<strong>Pastaba:</strong> Prašome apmokėti sąskaitą per savaitę nuo jos išrašymo datos.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>