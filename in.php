<?php
include ('inc/header.php');
$stmt = $db->prepare('SELECT turinys FROM kodai
				WHERE kodoID=:kodas');
$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
$kodo_turinys = $stmt->fetch(PDO::FETCH_ASSOC);
$kodo_turinys = $kodo_turinys['turinys'];
include("libs/geoip.inc");
$gi = geoip_open("libs/GeoIP.dat",GEOIP_STANDARD);
$country_name = geoip_country_name_by_addr($gi, $_SERVER['REMOTE_ADDR']);
//echo $country_name;
//die();

if (get_cfg_var('browscap'))
 $browser=get_browser(); //If available, use PHP native function
else
{
 require_once('libs/php-local-browscap.php');
 $browser=get_browser_local();
}

//print_r($browser);

//echo $kodo_turinys;
if ($kodo_turinys=='') { 
	$kodo_turinys = 'http://google.lt';
	}

if ($kodo_turinys!='http://google.lt') {
	$id = (int)$_GET['id'];
	if ($_COOKIE[$id]=='seen') {
		$stmt = $db->prepare('SELECT * FROM perziuros
				WHERE kodai_kodoID=:kodas AND diena=:data');
		$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
		$stmt->execute();
		if ($stmt->rowCount()>0) {
			// update neunikalios perziuros
			$stmt = $db->prepare('UPDATE perziuros SET neunikalios=neunikalios+1 
								WHERE kodai_kodoID=:kodas AND diena=:data');
			$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
			$stmt->execute();
		} else {
			// insert neunikalios perziuros
			$stmt = $db->prepare('INSERT INTO perziuros(kodai_kodoID, diena, neunikalios) VALUES (:kodas,:data,1)');
			$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
			$stmt->execute();
		}
	} else {
		setcookie($id,'seen',time()+60*60*24*7);
		$stmt = $db->prepare('SELECT * FROM perziuros
				WHERE kodai_kodoID=:kodas AND diena=:data');
		$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
		$stmt->execute();
		if ($stmt->rowCount()>0) {
			// update unikalios perziuros
			$stmt = $db->prepare('UPDATE perziuros SET unikalios=unikalios+1 
								WHERE kodai_kodoID=:kodas AND diena=:data');
			$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
			$stmt->execute();
		} else {
			// insert unikalios perziuros
			$stmt = $db->prepare('INSERT INTO perziuros(kodai_kodoID, diena, unikalios) VALUES (:kodas,:data,1)');
			$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindValue(':data', date("Y-m-d"), PDO::PARAM_STR);
			$stmt->execute();
		}

		$stmt = $db->prepare('SELECT * FROM salys
				WHERE kodai_kodoID=:kodas AND pavadinimas=:salis');
		$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindValue(':salis', $country_name, PDO::PARAM_STR);
		$stmt->execute();
		if ($country_name!='') {
			if ($stmt->rowCount()>0) {
				// update salis
				//echo "salis kiekis +";
				$stmt = $db->prepare('UPDATE salys SET kiekis=kiekis+1 
									WHERE kodai_kodoID=:kodas AND pavadinimas=:salis');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':salis', $country_name, PDO::PARAM_STR);
				$stmt->execute();
			} else {
				// insert salis
				$stmt = $db->prepare('INSERT INTO salys(kodai_kodoID, pavadinimas, kiekis) VALUES (:kodas,:salis,1)');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':salis', $country_name, PDO::PARAM_STR);
				$stmt->execute();
			}
		}

		$narsykle = $browser->browser;
		$stmt = $db->prepare('SELECT * FROM narsykles
				WHERE kodai_kodoID=:kodas AND pavadinimas=:narsykle');
		$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindValue(':narsykle', $narsykle, PDO::PARAM_STR);
		$stmt->execute();
		if ($narsykle!='') {
			if ($stmt->rowCount()>0) {
				// update narsykle
				//echo "narsyke +";
				$stmt = $db->prepare('UPDATE narsykles SET kiekis=kiekis+1 
									WHERE kodai_kodoID=:kodas AND pavadinimas=:narsykle');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':narsykle', $narsykle, PDO::PARAM_STR);
				$stmt->execute();
			} else {
				// insert narsykle
				$stmt = $db->prepare('INSERT INTO narsykles(kodai_kodoID, pavadinimas, kiekis) VALUES (:kodas,:narsykle,1)');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':narsykle', $narsykle, PDO::PARAM_STR);
				$stmt->execute();
			}
		}
		$platforma = $browser->platform;
		$stmt = $db->prepare('SELECT * FROM operacines
				WHERE kodai_kodoID=:kodas AND pavadinimas=:operacine');
		$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindValue(':operacine', $platforma, PDO::PARAM_STR);
		$stmt->execute();
		if ($platforma!='') {
			if ($stmt->rowCount()>0) {
				// update narsykle
				$stmt = $db->prepare('UPDATE operacines SET kiekis=kiekis+1 
									WHERE kodai_kodoID=:kodas AND pavadinimas=:operacine');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':operacine', $platforma, PDO::PARAM_STR);
				$stmt->execute();
			} else {
				// insert narsykle
				$stmt = $db->prepare('INSERT INTO operacines(kodai_kodoID, pavadinimas, kiekis) VALUES (:kodas,:operacine,1)');
				$stmt->bindValue(':kodas', $_GET['id'], PDO::PARAM_INT);
				$stmt->bindValue(':operacine', $platforma, PDO::PARAM_STR);
				$stmt->execute();
			}
		}

	}

}


header("location: ".$kodo_turinys);
geoip_close($gi);
?>